﻿using UnityEngine;
using System;
public class PlayerActor : MonoBehaviour {
    public static PlayerActor _instance;
    //public CharacterMovement characterMovement;
    public Transform projectileStartPosition;
    public AnimController animController;
    public AnalogStick inputMove;
    public SkillCanceller skillCanceller;
    public UniversalButton[] skillButtons;
    public SkillSetting[] skillSettings;
    public float[] cooldowns;

    public bool lerpStopping = false;
    public Transform dirMarker;
    public float moveSpeed;
    public float acceleration = 1f;
    public CharacterController controller;
    private float cachedMoveSpeed;
    private void Awake()
    {
        if (_instance == null) {
            _instance = this;
        }
        else {
            Destroy(gameObject);
        }
    }
    protected virtual void Start() {
        cachedMoveSpeed = moveSpeed;
        Application.targetFrameRate = 60;
        acceleration = 0f;
        cooldowns = new float[2];
        for (int i = 0; i < skillButtons.Length; i++) {
            skillButtons[i].SetActiveState(true);
            skillButtons[i].SetText("");
            skillButtons[i].onPointerDown.AddListener(OnSkillButtonPressed);
            skillButtons[i].onDrag.AddListener(OnSkillButtonDragged);
            skillButtons[i].onActivateSkill.AddListener(OnActivateSkill);
            skillButtons[i].onCancelSkill.AddListener(OnCancelSkill);

            skillSettings[i].skillMarker.transform.position = this.transform.position;
            skillSettings[i].skillMarker.SetActive(false);

            cooldowns[i] = 0f;

        }
        //dirMarker.gameObject.SetActive(false);
    }

    public Vector3 cachedInput;
    protected virtual void Update()
    {
        if (inputMove.isFingerDown)
        {
            cachedInput = inputMove.directionXZ;

            if (cachedInput != Vector3.zero)
            {
                transform.forward = cachedInput;
            }

        }
        else
        {
            if (lerpStopping)
            {
                cachedInput = Vector3.Lerp(cachedInput, Vector3.zero, moveSpeed * Time.deltaTime);
            }
            else
            {
                cachedInput = Vector3.zero;
            }
        }
        if (cachedInput.magnitude < 0.085)
        {
            dirMarker.gameObject.SetActive(false);
            cachedInput = Vector3.zero;
            moveSpeed = 0;
            animController.SetWalkFalse();
        }
        else
        {
            moveSpeed = cachedMoveSpeed;
            dirMarker.gameObject.SetActive(true);
            dirMarker.position = transform.position + cachedInput;
            //cachedInput = cachedInput.normalized;
            transform.Translate(cachedInput * moveSpeed * Time.deltaTime, Space.World);
        }
        //if (!controller.bake && animController.state != DragonState.Idle)
        //    UpdateCharacterMovement();
        //Vector3 dire = cameraSystem.cameraRotationTransform.rotation * characterMovement.cachedInput;
        //Vector3 dire = transform.right * cachedInput.x + transform.forward * cachedInput.z; //aqui
        //dirMarker.position = transform.position + dire * 2.5f;


        if (skillCanceller.isAnyFingerDown) {
            for (int i = 0; i < skillButtons.Length; i++) {
                skillSettings[i].skillMarker.transform.localPosition = GetSkillMarkerPosition(i);
            }
        }

        this.UpdateCooldown();
    }
    protected virtual void UpdateCooldown() {
        //Debug.Log(cooldowns.Length);
        for (int i = 0; i < cooldowns.Length; i++) {
            if (cooldowns[i] > 0f) {
                cooldowns[i] -= Time.deltaTime;
                if (cooldowns[i] < 1f) {
                    skillButtons[i].SetText(cooldowns[i].ToString("F1"));
                } else {
                    skillButtons[i].SetText("" + (int)cooldowns[i]);
                }


                if (skillButtons[i].state == UniversalButton.ButtonState.Active) {
                    skillButtons[i].SetActiveState(false);
                }
            } else {
                if (skillButtons[i].state == UniversalButton.ButtonState.Inactive) {
                    skillButtons[i].SetText("");
                    skillButtons[i].SetActiveState(true);
                }
            }
        }
    }

    protected virtual void OnSkillButtonPressed(int i) {
        skillSettings[i].skillMarker.SetActive(true);
        this.UpdateSkillMarkersState(i);
    }

    protected virtual void OnSkillButtonDragged(int i) {
        this.UpdateSkillMarkersState(i);
    }

    protected virtual void UpdateSkillMarkersState(int i) {
        if (skillCanceller.state == UniversalButton.ButtonState.Pressed) {
            skillSettings[i].SetMarkerCanCastSkill(false);
        } else {
            skillSettings[i].SetMarkerCanCastSkill(true);
        }
    }

    protected virtual void OnActivateSkill(int i) {
        controller.animController.UseAtk1();
        if(skillSettings[i].isFireball)
            skillSettings[i].SpawnFireBall(projectileStartPosition.position, skillSettings[i].skillMarker.transform.position + Vector3.up);
        else
            skillSettings[i].SpawnSkillAt(skillSettings[i].skillMarker.transform.position);
        skillSettings[i].skillMarker.SetActive(false);
        skillSettings[i].skillMarker.transform.position = this.transform.position;
        cooldowns[i] = skillSettings[i].cooldown;
        this.skillButtons[i].directionXZ = Vector3.zero;
    }

    protected virtual void OnCancelSkill(int i) {
        skillSettings[i].skillMarker.SetActive(false);
        skillSettings[i].skillMarker.transform.position = this.transform.position;
        this.skillButtons[i].directionXZ = Vector3.zero;
    }

    protected Vector3 GetSkillMarkerPosition(int i) {
        return transform.forward +Vector3.up*3 +skillButtons[i].directionXZ * skillSettings[i].range;//aqui
    }
    public void ResetPosition() {
        this.transform.position = Vector3.up;
    }

    [Serializable]
    public class SkillSetting {
        public bool isFireball;
        public GameObject skillPrefab;
        public float rotationSpeed;
        public float startingSize;
        public float sizeDecaySpeed;
        public float range;
        public GameObject skillMarker;
        public Material markerActivateSkillTrue;
        public Material markerActivateSkillFalse;
        public float cooldown;
        protected MeshRenderer renderer;

        //protected SkillRotatingBox skill;

        public void SetMarkerCanCastSkill(bool can) {
            if (renderer == null) {
                renderer = skillMarker.GetComponent<MeshRenderer>();
            }

            if (can) {
                renderer.material = markerActivateSkillTrue;
            } else {
                renderer.material = markerActivateSkillFalse;
            }
        }

        public void SpawnSkillAt(Vector3 position) {
            //skill = this.skillPrefab.GetComponent<SkillRotatingBox>();
            //skill.starSize = this.startingSize;
            //skill.sizeDecaySpeed = this.sizeDecaySpeed;

            Instantiate(skillPrefab, position, Quaternion.identity);

        }
        public void SpawnFireBall(Vector3 initialPosition, Vector3 finalPosition) {
            GameObject fireBall = Instantiate(skillPrefab, initialPosition, Quaternion.identity);
            Vector3 direction = (finalPosition - initialPosition).normalized;
            fireBall.GetComponent<Fireball>().finalPosition = finalPosition;
        }
    }
}
