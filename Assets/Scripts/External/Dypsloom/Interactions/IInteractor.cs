﻿namespace Controller
{
    /// <summary>
    /// The interactor allows you to interact with interactables.
    /// </summary>
    public interface IInteractor
    {
        void AddInteractable(IInteractable interactable);

        void RemoveInteractable(IInteractable interactable);
    }

    /// <summary>
    /// The character interactor has a reference to a character.
    /// </summary>
    public interface ICharacterInteractor : IInteractor
    {
        CharacterManager Character { get; }
    }
}