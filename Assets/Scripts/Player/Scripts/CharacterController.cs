﻿using Controller;
using System.Collections;
using UnityEngine;

public class CharacterController : Observer, IInteractor
{
    private AnalogStick joystick;
    public CharacterManager characterManage;
    public TouchAssistance touchAssistance;
    //public bool attrition;
    //public bool bake;
    public static CharacterController character;
    public AnimController animController;
    private Rigidbody rb;
    [SerializeField] private float jumpForce;
    [SerializeField] private float delayJumpForceTime;
    [SerializeField] private bool isJumping;
    [SerializeField] private bool isFlying;
    [SerializeField]private float flyEnergy = 100f;
    public UniversalButton jumpButton;
    private float up;
    private float right;

    private float jumpPressedTime = 0;

    private Coroutine waitToJumpRotine;
    public void Awake()
    {
        if (character == null)
        {
            character = this;
        }
        else {
            Destroy(gameObject);
        }
        rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        joystick = PlayerActor._instance.inputMove;
        touchAssistance.SubBtn2Pressed();
        flyEnergy = 100f;
    }

    public void Update()
    {
        if (joystick.directionXZ != Vector3.zero)
        {
            OnPlayerMoving();
        }
        else
        {
            OnPlayerStopMoving();
        }
    }
    public void ResetDirection() {
        joystick.directionXZ = Vector3.zero;
    }
    //public void FixedUpdate()
    //{
    //    if (bake) {
    //        rb.velocity = Vector3.zero;
    //    }
    //}
    IEnumerator DecreaseEnergy()
    {
        while (true)
        {
            flyEnergy--;
            if (flyEnergy <= 0) {
                StopFlyAction();
                StopCoroutine(DecreaseEnergy());
                break;
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void StopFlyAction() {
        OnPlayerStopFlying();
        if (isFlying) {
            animController.StopFlying();
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            flyEnergy = 100f;
            isFlying = false;
            if (animController.CheckIfIdle())
            {
                animController.state = DragonState.Idle;
            }
            else
            {
                animController.state = DragonState.Moving;
            }
        }
    }
    public void SetjumpPressedTime() {
        jumpPressedTime = Time.realtimeSinceStartup;
        waitToJumpRotine = StartCoroutine(WaitToJumpRotine());
    
    }

    IEnumerator WaitToJumpRotine() {
        yield return new WaitForSeconds(.5f);
        JumpAction();
    }

    public void JumpAction() {
        if (waitToJumpRotine != null) {
            StopCoroutine(WaitToJumpRotine());
        }

        //Debug.Log($"Tempo apertando o botão {Time.realtimeSinceStartup - jumpPressedTime}");
        OnPlayerJump();
        float force = Mathf.Lerp(0, jumpForce, (Time.realtimeSinceStartup - jumpPressedTime) * 5);
        //Debug.Log($"force {force}");
        if (!isJumping)
        {
            Vector3 direction = animController.lastDirXZ * force / 5;
            if (animController.state == DragonState.Moving)
                rb.AddForce(new Vector3(0, force, 0) + transform.forward * force / 8);
            else
                rb.AddForce(new Vector3(0, force * 1.2f, 0));
            isJumping = true;
            animController.Jump();
            StartCoroutine(Flying());
        }
        else {

        }
    }

    IEnumerator Flying()
    {
        yield return new WaitForSeconds(0.25f);
        while (true)
        {
            yield return new WaitForSeconds(0.05f);
            if (!isJumping)
            {
                
                isFlying = false;
                StopCoroutine(Flying());
                break;
            }
            else if (!jumpButton.isFingerDown) {
                StopFlyAction();
                continue;
            }

            if (rb.velocity.y < 0)
            {
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                isFlying = true;
                animController.Fly();
            }
        }
    }
    public void DelayedIdleJump()
    {
        if (!isJumping)
        {
            animController.Jump();
            Invoke("AddJumpForce", delayJumpForceTime);
        }
    }

    public void AddJumpForce()
    {
        rb.AddForce(new Vector3(0, jumpForce*1.3f, 0));
        isJumping = true;
        StartCoroutine(Flying());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground")) {
            if (isFlying) {
                StopFlyAction();
                isFlying = false;
            }

            if (isJumping)
            {
                isJumping = false;
                animController.SetJumpToFalse();
            }
        }
        else if (collision.gameObject.CompareTag("Lava"))
        {
            isJumping = true;
            animController.Jump();
            rb.velocity = Vector3.zero;
            rb.AddForce(new Vector3(0,jumpForce *2,0));
            Invoke("ReduceImpact", 0.15f);
            characterManage.LoseLife(1);
        }
    }
    public void ReduceImpact() {
        rb.velocity = Vector3.zero;
    }
/*    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Water")) {
            //isJumping = false;
            rb.velocity = new Vector3(0, -0.5f, 0);
            animController.Jump();
        }
    }*/
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Death")) {
            characterManage.Die();
        }
        if (other.CompareTag("Enemy")) {
            characterManage.LoseLife(1);
            Vector3 direction = (other.transform.position - transform.position).normalized;
            rb.AddForce(new Vector3(direction.x, 0.15f, direction.z) * 200);
        }
        if (other.CompareTag("Ground")) {
            //rb.AddForce((((other.transform.position - transform.position).normalized) + Vector3.down*0.3f) * -400);
            rb.AddRelativeForce((-Vector3.forward + Vector3.down*0.4f) * 400f);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if(isFlying)
            rb.velocity = Vector3.zero;
    }

    public void AddInteractable(IInteractable interactable)
    {
        Debug.Log("Interagindo");
    }

    public void RemoveInteractable(IInteractable interactable)
    {
        Debug.Log("saindo");
    }
}
public enum DragonState {Idle,Moving,Flying, Drown };
