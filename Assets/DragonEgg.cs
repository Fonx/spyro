﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonEgg : MonoBehaviour
{
    [Range(1,3)]
    public int eggNumber;
    public bool taked;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            collision.gameObject.GetComponent<CharacterManager>().GetEgg(eggNumber);
            Destroy(gameObject);
        }
    }
}
