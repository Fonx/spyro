﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraOptions : MonoBehaviour
{
    private Transform CameraTransform;
    public CollisionType mode;
    public CameraOptions[] cameraOptions;
    private bool ChangeclimppingPlanes;
    //private bool AddCameraCullingMask;
    //private bool RemoveCameraCullingMask;
    private bool CanChangeCameraPosition;
    public Camera cam;
    public bool TurnOnOldConfig;
    public string[] layersToAdd;
    public string[] layersToRemove;
    public Vector3 NewCameraPosition;
    private Vector3 initialCameraPosition;
    private Vector2 oldClippingPlanes;
    [SerializeField]private Vector2 climppingPlanes = new Vector2 (0,100);
    private int oldCullingMask;
    void Start()
    {
        CameraTransform = cam.transform;
        GetScriptProperties();
        oldCullingMask = cam.cullingMask;
        initialCameraPosition = CameraTransform.localPosition;
        oldClippingPlanes = new Vector2(cam.nearClipPlane, cam.farClipPlane);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void GetScriptProperties() {
        for (int i = 0; i < cameraOptions.Length; i++) {
            if (cameraOptions[i] == CameraOptions.ChangeclimppingPlanes) {
                ChangeclimppingPlanes = true;
                continue;
            }
            //if (cameraOptions[i] == CameraOptions.AddCameraCullingMask) {
            //    AddCameraCullingMask = true;
            //    continue;
            //}
            //if (cameraOptions[i] == CameraOptions.RemoveCameraCullingMask) {
            //    RemoveCameraCullingMask = true;
            //    continue;
            //}
            if (cameraOptions[i] == CameraOptions.ChangeCameraPosition) {
                CanChangeCameraPosition = true;
                continue;
            }
        }
    }

    private void TurnOnSomeMask(string maskName)
    {
        cam.cullingMask |= 1 << LayerMask.NameToLayer(maskName);
    }
    private void TurnOffSomeMask(string maskName) {
        cam.cullingMask &= ~(1 << LayerMask.NameToLayer(maskName));
    }
    private void ToggleSomeMask(string maskName) {
        cam.cullingMask ^= 1 << LayerMask.NameToLayer(maskName);
    }
    public void ChangeClimppingPlanes() {
        cam.nearClipPlane = climppingPlanes.x;
        cam.farClipPlane = climppingPlanes.y;
    }
    public void AddCullingMaskLayers() {
        for (int i = 0; i < layersToAdd.Length; i++) {
            TurnOnSomeMask(layersToAdd[i]);
        }
    }
    public void RemoveCullingMaskLayers() {
        for (int i = 0; i < layersToRemove.Length; i++)
        {
            TurnOffSomeMask(layersToRemove[i]);
        }
    }
    public void ChangeCameraPositionToNewPosition() {
        //CameraTransform.localPosition = NewCameraPosition;
        StartCoroutine(ChangeCamepraPositionSmooth(NewCameraPosition));
    }
    IEnumerator ChangeCamepraPositionSmooth(Vector3 finalPos) {
        while (true) {
            CameraTransform.localPosition = Vector3.MoveTowards(CameraTransform.localPosition, finalPos, 80 * Time.deltaTime);
            if (CameraTransform.localPosition == finalPos) {
                yield break;
            }
            yield return new WaitForSeconds(0.05f);
        }
    }
    public void OldConfigurations() {
        //cam.cullingMask = oldCullingMask;
        StartCoroutine(ChangeCamepraPositionSmooth(initialCameraPosition));
        cam.nearClipPlane = oldClippingPlanes.x;
        cam.farClipPlane = oldClippingPlanes.y;
    }
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (!collision.gameObject.CompareTag("Player")) {
    //        return;
    //    }
    //    if (ChangeclimppingPlanes) {
    //        ChangeClimppingPlanes();
    //    }
    //    if (AddCameraCullingMask) {
    //        AddCullingMaskLayers();
    //    }
    //    if (RemoveCameraCullingMask)
    //    {
    //        RemoveCullingMaskLayers();
    //    }
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            return;
        }
        if (TurnOnOldConfig) {
            OldConfigurations();
            return;
        }
        if (ChangeclimppingPlanes) {
            ChangeClimppingPlanes();
        }
        //if (AddCameraCullingMask)
        //{
        //    AddCullingMaskLayers();
        //}
        //if (RemoveCameraCullingMask) {
        //    RemoveCullingMaskLayers();
        //}
        if (CanChangeCameraPosition) {
            ChangeCameraPositionToNewPosition();
        }
    }
}
public enum CameraOptions { ChangeclimppingPlanes,  ChangeCameraPosition }//,AddCameraCullingMask,RemoveCameraCullingMask}
public enum CollisionType { Trigger }//,Collision}