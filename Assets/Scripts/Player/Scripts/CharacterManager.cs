﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterManager : MonoBehaviour
{
    public Renderer rend;
    public bool cantLoseLife;
    public AnimController animController;
    public Rigidbody rb;
    private Vector3 oldPos;
    private RigidbodyConstraints oldConstraints;

    public void LoseLife(int life) {
        if(!cantLoseLife)
            StartCoroutine(Damaged());
    }
    public void Die() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    IEnumerator Damaged()
    {
        cantLoseLife = true;
        for (int i=0;i<20;i++) {
            rend.enabled = false;
            yield return new WaitForSeconds(0.05f);
            rend.enabled = true;
            yield return new WaitForSeconds(0.05f);
        }
        rend.enabled = true;
        cantLoseLife = false;
    }

    public void GetEgg(int eggNumer) {
        animController.GetEgg();
        //movement.cantmove = true;
        oldConstraints = rb.constraints;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        oldPos = transform.position;
        transform.position = oldPos + Vector3.up*3;
        cantLoseLife = true;
    }
    public void FixPositionAfterEgg()
    {
        transform.position = oldPos;
        //movement.cantmove = false;
        cantLoseLife = false;
        rb.constraints = oldConstraints;
    }
}
