﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeckTargetController : MonoBehaviour
{
    public Transform neckTransform;
    public Transform targetRotation;
    private Quaternion initialRotation;
    public bool applyRotation;

    void Start()
    {
        initialRotation = neckTransform.rotation;
        applyRotation = false;
    }

    private void LateUpdate()
    {
        if (applyRotation)
        {
            Quaternion lookRotation = Quaternion.LookRotation(targetRotation.position - neckTransform.position);
            Quaternion rotationToGo = lookRotation * initialRotation;
            neckTransform.rotation = rotationToGo;
            if (neckTransform.localRotation.eulerAngles.y < 220 && neckTransform.localRotation.eulerAngles.y > 180)
            {
                neckTransform.localRotation = Quaternion.Euler(neckTransform.localRotation.eulerAngles.x, 220, neckTransform.localRotation.eulerAngles.z);
            }
            else if (neckTransform.localRotation.eulerAngles.y > 100 && neckTransform.localRotation.eulerAngles.y < 180)
            {
                neckTransform.localRotation = Quaternion.Euler(neckTransform.localRotation.eulerAngles.x, 100, neckTransform.localRotation.eulerAngles.z);
            }
        }
    }
    public void SetTarget(Transform target) {
        targetRotation = target;
        applyRotation = true;
    }
    public void SetFlagToOff() {
        applyRotation = false;
    }
}
