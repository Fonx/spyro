﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public string levelName;
    public DragonEgg[] eggs;
    void Start()
    {
        StartDragonEggs();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartDragonEggs() {
        for (int i = 0; i < eggs.Length; i++) {
            if (eggs[i].taked) {
                eggs[i].gameObject.SetActive(false);
            }
        }
    }
}
