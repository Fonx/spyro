﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallCube : MonoBehaviour
{
    public float distance;
    public float fallingSpeed;
    public float delay;
    public bool recurrent;
    private Vector3 initialPosition;
    private bool CR_running;
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            Invoke("StartCourotineToFall", delay);
        }
    }
    private void Start()
    {
        initialPosition = transform.position;
    }
    public void StartCourotineToFall() {
        if (!CR_running)
        {
            StartCoroutine(StartFalling());
            CR_running = true;
        }
    }
    IEnumerator StartFalling() {
        while (true)
        {
            transform.position = Vector3.MoveTowards(transform.position, transform.position - Vector3.up * distance, Time.deltaTime * fallingSpeed);
            if (transform.position.y <= initialPosition.y - distance) {
                if (recurrent)
                {
                    transform.position = initialPosition;
                    StopCoroutine(StartFalling());
                    CR_running = false;
                }
                else
                    Destroy(gameObject);

                break;
            }
            yield return new WaitForSeconds(0.08f);
        }
    }
}
