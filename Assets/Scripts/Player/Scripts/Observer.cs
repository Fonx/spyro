﻿using System;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public event Action Move = delegate { };
    public event Action StopMove = delegate { };
    public event Action Jump = delegate { };
    public event Action Fly = delegate { };
    public event Action StopFly = delegate { };

    protected void OnPlayerMoving() {
        Move.Invoke();
    }
    protected void OnPlayerStopMoving() {
        StopMove.Invoke();
    }
    protected void OnPlayerJump() {
        Jump.Invoke();
    }
    protected void OnPlayerFlying() {
        Fly.Invoke();
    }
    protected void OnPlayerStopFlying() {
        StopFly.Invoke();
    }
}
