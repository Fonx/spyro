﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour {
    public bool pushOnAwake = true;
    public Vector3 startDirection;
    public float startMagnitude;
    public ForceMode forceMode;

    public GameObject fieryEffect;
    public GameObject smokeEffect;
    public GameObject explodeEffect;

    public Vector3 finalPosition;
    public float speed;
    public void Awake()
    {
        //rgbd = GetComponent<Rigidbody>();
        StartCoroutine(SlowUpdate());
    }

    public void Start()
    {
        //if (pushOnAwake)
        //{
        //    Push(startDirection, startMagnitude);
        //}

    }
    IEnumerator SlowUpdate()
    {
        while (true)
        {
            transform.position = Vector3.MoveTowards(transform.position, finalPosition, speed * Time.deltaTime);
            if (transform.position == finalPosition)
            {
                StartCollision();
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void Push(Vector3 direction, float magnitude)
    {
        Vector3 dir = direction.normalized;
        //rgbd.AddForce(dir * magnitude, forceMode);
    }

    public void OnCollisionEnter(Collision col)
    {
        Debug.Log(col.gameObject.name);
        //rgbd.Sleep();
        if (col.gameObject.CompareTag("Player"))
            return;
        Debug.Log(col.gameObject.name);
        StartCollision();
    }
    public void StartCollision() {
        if (fieryEffect != null)
        {
            StopParticleSystem(fieryEffect);
        }
        if (smokeEffect != null)
        {
            StopParticleSystem(smokeEffect);
        }
        if (explodeEffect != null)
            explodeEffect.SetActive(true);
        Destroy(gameObject, 0.4f);
    }
    public void StopParticleSystem(GameObject g)
    {
        ParticleSystem[] par;
        par = g.GetComponentsInChildren<ParticleSystem>();
        foreach(ParticleSystem p in par)
        {
            p.Stop();
        }
    }

    public void OnEnable()
    {
        if (fieryEffect != null)
            fieryEffect.SetActive(true);
        if (smokeEffect != null)
            smokeEffect.SetActive(true);
        if (explodeEffect != null)
            explodeEffect.SetActive(false);
    }
}


