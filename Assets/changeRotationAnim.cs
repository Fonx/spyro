﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeRotationAnim : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.Find("DragonShape").localRotation = Quaternion.Euler(-20, 0, 0);
    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.Find("DragonShape").localRotation = Quaternion.identity;
    }
}
