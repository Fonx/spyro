﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public CharacterController controller;
    public Vector3 cachedInput;
    public float moveSpeed;
    public bool cantmove;
    public Collider horn;
    //public CameraSystem camera;
    public void Update()
    {
        //UpdateCharacterMovement();
        if (cachedInput.z > 0.1f) {
            horn.enabled = true;
        }
        else {
            horn.enabled = false;
        }
    }
}
