﻿using System.Collections;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    private CharacterController controller;
    public PlayerActor actor;
    public float DirectionDampTime = .15f;
    public Animator anim;
    //public float speedMagnitude;
    public float actorAccelaration;
    public DragonState state;
    public Vector3 lastDirXZ;

    //private float angleA;
    //private float angleB;
    void Start()
    {
        controller = GetComponent<CharacterController>();
        SubscribePlayer();
        InvokeRepeating("GetPreviousDir", 0.25f, 0.25f);
        StartCoroutine(SlowUpdate());

        state = DragonState.Idle;
    }
    void Update()
    {
        //Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        //pos.x = Mathf.Clamp01(pos.x);
        //pos.y = Mathf.Clamp01(pos.y);
        //transform.position = Camera.main.ViewportToWorldPoint(pos);
    }
    private void SubscribePlayer() {
        controller.Move += SetWalkTrue;
        controller.StopMove += SetWalkFalse;
    }
    IEnumerator SlowUpdate()
    {
        while (true)
        {
            //float direction = ReturnCorrectAngle();
            //anim.SetFloat("direction", actor.cachedInput.x, DirectionDampTime, Time.deltaTime);
            anim.SetFloat("speed", actor.cachedInput.magnitude);
            yield return new WaitForSeconds(0.1f);
        }
    }
    public bool CheckIfIdle() {
        if (actor.cachedInput.magnitude >0)
        {
            return true;
        }
        else
            return false;
    }
    //public float ReturnCorrectAngle() {
    //    angleA = Mathf.Atan2(Quaternion.Euler(lastDirXZ).x, Quaternion.Euler(lastDirXZ).z) * Mathf.Rad2Deg;
    //    angleB = Mathf.Atan2(Quaternion.Euler(directionXZ).x, Quaternion.Euler(directionXZ).z) * Mathf.Rad2Deg;
    //    return Mathf.DeltaAngle(angleA, angleB);
    //}
    public void GetPreviousDir() {
        lastDirXZ = actor.inputMove.directionXZ;
    }
    public void Jump() {
        anim.SetBool("jump",true);
    }
    public void Hurt()
    {
        anim.SetTrigger("hurt");
    }
    public void SetJumpToFalse() {
        anim.SetBool("jump", false);
    }
    public void Fly() {
        state = DragonState.Flying;
        anim.SetBool("fly", true);
    }
    public void StopFlying() {
        anim.SetBool("fly", false);
    }
    public void SetWalkTrue() {
        if (state == DragonState.Idle)
        {
            state = DragonState.Moving;
            anim.SetBool("walk", true);
        }
    }
    public void SetWalkFalse() {

        if (state == DragonState.Moving)
        {
            state = DragonState.Idle;
            anim.SetBool("walk", false);
        }
        //Debug.Log(state);
    }
    public void UseAtk1() { // aqui
        anim.SetFloat("atk", 1f);
        Invoke("ResetAtkTrigger", 2.0f);
    }
    public void UseSS1() {
        anim.SetTrigger("ss1");
    }
    public void ResetAtkTrigger() {
        anim.SetFloat("atk", 0);
    }
    public void GetEgg() {
        anim.SetTrigger("getegg");
    }
}
